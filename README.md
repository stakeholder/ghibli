# GhiBli
A python application serving movies from Studio Ghibli api


## Installation
1. Start up your terminal (or Command Prompt on Windows OS).
2. Ensure that you've `python` and `redis` installed on your PC.
3. Clone the repository.
4. Navigate to the project folder using `cd into the project folder` on your terminal (or command prompt)
5. After cloning, create a virtual environment then install the requirements with the command:
`pip install -r requirements.txt`.
6. Create a super user using the command
    `python manage.py createsuperuser`
7. After creating the `.env`, Setup up your database and start the application running the command: 
    `python manage.py wait_for_db && python manage.py migrate && python manage.py runserver`
8. Start your redis with the command
    `redis-server`
9. Start celery with the command
    `celery -A movielist worker -l INFO`
10. Start celery beat using the command
    `celery -A movielist beat -l DEBUG --max-interval=<value>`

NB:
A periodic scheduled task was setup using celery. The task checks the Studio Ghibli api for new movie releases and updates the local database accordingly.

## Scheduling the Periodic task using django admin
1. Log into django admin page using the superuser credentials created above.
![image](https://gitlab.com/stakeholder/ghibli/raw/master/images/Screenshot%202019-10-24%20at%206.31.12%20AM.png)

2. We need to create an interval by following these steps:

    a. click on `Intervals` on the page shown below.
    ![image](https://gitlab.com/stakeholder/ghibli/raw/master/images/Screenshot%202019-10-24%20at%206.31.29%20AM.png)

    b. On the Intervals page, click on `Add Interval button` at the top right.
    ![image](https://gitlab.com/stakeholder/ghibli/raw/master/images/Screenshot%202019-10-24%20at%207.03.00%20AM.png)

    c. Fill in the `number of periods` and `interval period` as shown below and click `save`.
    ![image](https://gitlab.com/stakeholder/ghibli/raw/master/images/Screenshot%202019-10-24%20at%207.04.20%20AM.png)


3. We now need to setup the Periodic Task by following these steps:

    a. On the homepage click on `Periodic tasks`
     ![image](https://gitlab.com/stakeholder/ghibli/raw/master/images/Screenshot%202019-10-24%20at%206.31.29%20AM.png)

    b. On the Intervals page, click on `Add Periodic Task button` at the top right.
    ![image](https://gitlab.com/stakeholder/ghibli/raw/master/images/Screenshot%202019-10-24%20at%206.33.57%20AM.png)

    c. Fill in the required information:

        i.   Fill in the task name

        ii.  Select the task in the dropdown

        iii. Fill in description (optional)

        iv.  Under the Schedule section, select the `Interval Schedule` from the dropdown
        
        v.   Click on the `save button`.
    ![image](https://gitlab.com/stakeholder/ghibli/raw/master/images/Screenshot%202019-10-24%20at%207.23.52%20AM.png)
