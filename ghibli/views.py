from django.shortcuts import render

from core.utils import repopulate_table_with_api_data

from core.models import Film, Person
from core.serializers import (
    FilmInputSerializer,
    FilmOutputSerializer,
    PersonInputSerializer
)


MOVIE_URL = 'https://ghibliapi.herokuapp.com/films'
PEOPLE_URL = 'https://ghibliapi.herokuapp.com/people'


def movies(request):
    try:
        if not Film.objects.count():
            repopulate_table_with_api_data(MOVIE_URL,
                                           FilmInputSerializer, Film)
            repopulate_table_with_api_data(PEOPLE_URL,
                                           PersonInputSerializer, Person)

        movies = Film.objects.all().order_by('-release_date')

        serializer = FilmOutputSerializer(movies, many=True)

        context = {'films': serializer.data}

        return render(request, 'ghibli/movies.html', context)
    except Exception as err:
        print(f'An unexpected error occurred {err}')
        return render(request, 'ghibli/error.html', {})
