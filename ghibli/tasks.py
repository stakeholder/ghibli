from movielist import celery_app

from core.utils import (
    update_db_with_new_movies_from_api,
    repopulate_table_with_api_data
)
from core.models import (
    Film,
    Person
)
from core.serializers import (
    FilmInputSerializer,
    PersonInputSerializer
)


MOVIE_URL = 'https://ghibliapi.herokuapp.com/films'
PEOPLE_URL = 'https://ghibliapi.herokuapp.com/people'


@celery_app.task
def update_database_with_latest_data():
    update_db_with_new_movies_from_api(MOVIE_URL, FilmInputSerializer, Film)
    repopulate_table_with_api_data(PEOPLE_URL, PersonInputSerializer, Person)
