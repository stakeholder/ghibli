from django.contrib import admin

from .models import (
    Film,
    Person
)

admin.site.register(Film)
admin.site.register(Person)
