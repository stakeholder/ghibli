from django.db import models
from django.utils import timezone


class Film(models.Model):
    """Film object"""
    id = models.CharField(max_length=255, primary_key=True)
    title = models.CharField(max_length=255)
    description = models.TextField()
    director = models.CharField(max_length=255, blank=True)
    producer = models.CharField(max_length=255,  blank=True)
    release_date = models.CharField(max_length=255,  blank=True)
    rt_score = models.CharField(max_length=255,  blank=True)
    url = models.URLField()
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title


class Person(models.Model):
    """Person Object"""
    id = models.CharField(max_length=255, primary_key=True)
    name = models.CharField(max_length=255)
    gender = models.CharField(max_length=255,  blank=True)
    age = models.CharField(max_length=255,  blank=True)
    eye_color = models.CharField(max_length=255,  blank=True)
    hair_color = models.CharField(max_length=255,  blank=True)
    url = models.URLField()
    films = models.ManyToManyField('Film')

    def __str__(self):
        return self.name
