import requests
import pandas as pd
from requests.exceptions import HTTPError


def fetch_resource(url):
    try:
        response = requests.get(url)
        response.raise_for_status()
        return response.json()
    except HTTPError as http_err:
        print(f'Http error occurred: {http_err}')
    except Exception as err:
        print(f'An error occurred: {err}')


def persist_data_in_db(data, serializer_class, many=True):
    serializer = serializer_class(data=data, many=True)
    if serializer.is_valid():
        serializer.save()
        print('Done updating database')
    else:
        print(f'Validation Error: {serializer.errors}')


def update_db_with_new_movies_from_api(url, serializer_class, model):
    data = fetch_resource(url)
    df = pd.DataFrame(data)
    data_ids = df['id'].tolist() if not df.empty else []

    if data_ids:
        movies_from_db = model.objects.all()
        serializer = serializer_class(movies_from_db, many=True)
        movies_df = pd.DataFrame(serializer.data)
        old_movies_ids = \
            movies_df['id'].tolist() if not movies_df.empty else []

        new_movies_ids = list(
            set(data_ids).difference(set(old_movies_ids))
        )

        if new_movies_ids:
            latest_movie_df = df.loc[df['id'].isin(new_movies_ids)]
            new_movies = latest_movie_df.to_dict(orient='records')
            persist_data_in_db(new_movies, serializer_class)


def repopulate_table_with_api_data(url, serializer_class, model):
    model.objects.all().delete()
    data = fetch_resource(url)

    if data:
        persist_data_in_db(data, serializer_class)
