from unittest.mock import patch, Mock

from django.test import TestCase

from core.utils import (
    fetch_resource,
    persist_data_in_db,
    repopulate_table_with_api_data
)
from core.serializers import FilmInputSerializer
from core.models import Film


def sample_film(film_id, title="The lost island",
                description="A misplaced island",
                director="Hollywood Director",
                producer="Hollywood Producer",
                release_date="1985", rt_score="96",
                url="http://sample.com"):
    """Create sample film"""
    return {
        'id': film_id,
        'title': title,
        'description': description,
        'director': director,
        'producer': producer,
        'release_date': release_date,
        'rt_score': rt_score,
        'url': url
    }


class UtilsTest(TestCase):

    def setUp(self):
        self.response_mock = Mock()
        self.response_mock.status_code = 200
        self.response_mock.json.return_value = [sample_film('345234523')]

    def test_fetch_resource(self):
        """Test fetching resource from external api successfully"""
        with patch('core.utils.requests.get') as req:
            req.return_value = self.response_mock
            response = fetch_resource('http://sample.com')
            self.assertEqual(response[0]['title'], 'The lost island')

    def test_persist_data_in_db(self):
        """Test persisting data in database"""
        sample_movie = sample_film('123255-352523')
        persist_data_in_db([sample_movie], FilmInputSerializer)

        movie_from_db = Film.objects.all()

        self.assertEqual(len(movie_from_db), 1)
        self.assertEqual(movie_from_db.first().title, sample_movie['title'])

    def test_repopulate_table_with_api_data(self):
        """
        Test database is repopulated if data was
        successful fetched from api
        """
        Film.objects.create(
            **sample_film('23252352934')
        )

        with patch('core.utils.requests.get') as req:
            req.return_value = self.response_mock

            repopulate_table_with_api_data('http://sample.com',
                                           FilmInputSerializer,
                                           Film)
            self.assertNotEqual(Film.objects.count(), 2)
            self.assertEqual(Film.objects.first().title, 'The lost island')
