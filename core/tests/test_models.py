from django.test import TestCase

from core import models


def sample_film(film_id, title="The lost island",
                description="A misplaced island",
                director="Hollywood Director",
                producer="Hollywood Producer",
                release_date="1985", rt_score="96"):
    """Create sample film"""
    return models.Film.objects.create(
        id=film_id,
        title=title,
        description=description,
        director=director,
        producer=producer,
        release_date=release_date,
        rt_score=rt_score
    )


def sample_person(person_id, name="Stranger",
                  gender="male", age="unknown",
                  eye_color="brown", hair_color="brown",
                  url="https://sample.com/52454252"):
    """Create sample person"""
    return models.Person.objects.create(
        id=person_id,
        name=name,
        gender=gender,
        age=age,
        eye_color=eye_color,
        hair_color=hair_color,
        url=url
    )


class ModelTests(TestCase):

    def test_film_str(self):
        """Test the film string representation"""
        film = sample_film("1234-5678")

        self.assertEqual(str(film), film.title)

    def test_create_new_film(self):
        """Test creating a new film is successful"""
        film_id = "34545-345252"
        sample_film(film_id)

        film_list = models.Film.objects.all()

        self.assertEqual(len(film_list), 1)
        self.assertEqual(film_list[0].id, film_id)

    def test_person_str(self):
        """Test the person string representation"""
        person = sample_person("122242-r45234")

        self.assertEqual(str(person), person.name)

    def test_create_new_person(self):
        """Test creating a new person is successful"""
        person_id = "34545-345252"
        sample_person(person_id)

        person_list = models.Person.objects.all()

        self.assertEqual(len(person_list), 1)
        self.assertEqual(person_list[0].id, person_id)
