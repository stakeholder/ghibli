from rest_framework import serializers

from core.models import Film, Person


class FilmInputSerializer(serializers.ModelSerializer):
    """Serializer for film object"""
    class Meta:
        model = Film
        fields = '__all__'


class FilmOutputSerializer(serializers.Serializer):
    """Serializer for film output object"""
    title = serializers.CharField()
    people = serializers.SerializerMethodField()

    def get_people(self, obj):
        return obj.person_set.all()


class PersonInputSerializer(serializers.ModelSerializer):
    """Serializer for person object"""
    films = serializers.ListField(child=serializers.URLField())

    class Meta:
        model = Person
        fields = '__all__'

    def create(self, validated_data):
        movies = Film.objects.filter(url__in=validated_data.pop('films'))
        person = Person.objects.create(**validated_data)
        person.films.set(movies)
        return person


class PersonOutputSerializer(serializers.ModelSerializer):
    """Serializer for person output object"""

    class Meta:
        model = Person
        exclude = ['films']
